<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/autoriser');

function sync_from_forge_autoriser_travaux($flux) {

    if (_request('page') == 'sync_from_forge') {
        $flux['data'] = true;
    }
    return $flux;
}

function sync_from_forge_affichage_final($flux) {

    //Désactiver la surcharge graphique de en_travaux
    if (_request('page') == 'sync_from_forge') {
        $GLOBALS['html'] = false;
    }
    return $flux;
}