# Sync from forge

Ce plugin permet de synchroniser un répertoire local du site avec une forge externe accessible via Git.
Il est possible de faire appel à une page dédiée (webhook) pour synchroniser localement le dépôt.

## Installation

Devrait s'installer comme n'importe quel plugin.
Il faut que le site
* soit autorisé à utiliser la commande PHP [exec()](https://www.php.net/manual/en/function.exec.php)
* que l'exécutable git soit disponible.

## Configuration

Le plugin prend en compte une seule synchronisation git.
Le répertoire local doit être déjà synchronisé avec le projet git (via http ou ssh)


Depuis la page de configuration du plugin, pensez à indiquer :
* un jeton à donner au webhook
* le chemin relatif par rapport à la racine du site, par exemple '''/squelettes/''' ou '''/plugins/sync_from_forge'''
* le chemin du dépôt source, par exemple une URL HTTP avec les paramètres de connexion utiles.*


## Fonctionnement

Lors d'un appel à la page http://mon.site/?page=sync_from_forge&token=[#TOKEN], le plugin fera une vérification de validité du token et lancer un pull sur le dépot déclaré dans le répertoire local


# TODO

* Initialiser à la volée le répertoire si celui ci est manquant ou vide
* Permettre de déclarer plusieurs répertoires à suivre
* Préciser pour chaque dépôt la branche à suivre
* Gérer le stash si des modifications locales sont présentes
* Détecter les copies locales existantes


# Licence et autres

Le projet source est hébergé sur https://git.webelys.com/spip/sync_from_forge,
Une copie est proposée sur sur https://git.spip.net/spip-contrib-extensions/sync_from_forge

L'auteur est Camille Lafitte pour le compte de la société Webelys

La licence est [GPLv3](https://www.gnu.org/licenses/gpl-3.0.fr.html)